# README #
Created at Dragon Hacks 2015: http://challengepost.com/software/the-light-painter-s-palette-q5zrh

## Project Description
Existing light painting practices make the art both difficult and inflexible. Even following the position of your own hand is next to impossible, let alone creating more advanced pieces incorporating multiple colors or intensities of light. The Light Painter’s Palette greatly simplifies this process by overlaying digital media in the real world to bring your artwork to the next level.

## Hardware Used
* Intel Edison running Node.js
    * Edison hooked up to a breadboard with an RGB LED
* Microsoft Kinect v2
