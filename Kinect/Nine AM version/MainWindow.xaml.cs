﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.BodyBasics
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Microsoft.Kinect;
    using System.Runtime.InteropServices;
    using System.Net.Sockets;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    //using System.Drawing;
    using Emgu.CV;
    using Emgu.CV.Structure;

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Radius of drawn hand circles
        /// </summary>
        private const double HandSize = 30;

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Thickness of clip edge rectangles
        /// </summary>
        private const double ClipBoundsThickness = 10;

        /// <summary>
        /// Constant for clamping Z values of camera space points from being negative
        /// </summary>
        private const float InferredZPositionClamp = 0.1f;

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as closed
        /// </summary>
        private readonly Brush handClosedBrush = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as opened
        /// </summary>
        private readonly Brush handOpenBrush = new SolidColorBrush(Color.FromArgb(128, 0, 255, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as in lasso (pointer) position
        /// </summary>
        private readonly Brush handLassoBrush = new SolidColorBrush(Color.FromArgb(128, 128, 128, 255));

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Drawing group for body rendering output
        /// </summary>
        private System.Windows.Media.DrawingGroup drawingGroup;

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Reader for body frames
        /// </summary>
        private BodyFrameReader bodyFrameReader = null;

        /// <summary>
        /// Array for the bodies
        /// </summary>
        private Body[] bodies = null;

        /// <summary>
        /// definition of bones
        /// </summary>
        private List<Tuple<JointType, JointType>> bones;

        /// <summary>
        /// Width of display (depth space)
        /// </summary>
        private int displayWidth;

        /// <summary>
        /// Height of display (depth space)
        /// </summary>
        private int displayHeight;

        /// <summary>
        /// List of colors for each body tracked
        /// </summary>
        private List<Pen> bodyColors;

        /// <summary>
        /// Current status text to display
        /// </summary>
        private string statusText = null;

        /// <summary>
        /// Reader for color frames
        /// </summary>
        private ColorFrameReader colorFrameReader = null;

        /// <summary>
        /// Bitmap to display
        /// </summary>
        private WriteableBitmap colorBitmap = null;

        private Image<Bgra,byte> finalImage = null;

        private Image<Bgra, byte> last = null;

        private int imagesCaptured = 0;

        private string currentImage = null;

        private BitmapImage image;

        private UdpClient server;
        
        private IPEndPoint remoteIPEndPoint = null;

        private Point? oldPos = null;

        private Queue<Point> trail = new Queue<Point>(50);

        public static Color GetPixelColor(BitmapSource source, int x, int y) {
            Color c = Colors.White;
            if (source != null) {
                try {
                    CroppedBitmap cb = new CroppedBitmap(source, new Int32Rect(x, y, 1, 1));
                    var pixels = new byte[4];
                    cb.CopyPixels(pixels, 4, 0);
                    c = Color.FromRgb(pixels[2], pixels[1], pixels[0]);
                } catch (Exception) { }
            }
            return c;
        }

        private Point imageLocation = new Point(200,20);

        private double imageScale = 1.0;

        private Point grabStartPoint;

        private double grabStartDist;

        private bool isDragging = false;

        private bool isScaling = false;

        private bool imageStartTrigger = false;

        private bool imageEndTrigger = false;

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow() {
            // one sensor is currently supported
            this.kinectSensor = KinectSensor.GetDefault();

            // get the coordinate mapper
            this.coordinateMapper = this.kinectSensor.CoordinateMapper;

            // get the depth (display) extents
            FrameDescription frameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;

            // get size of joint space
            this.displayWidth = frameDescription.Width;
            this.displayHeight = frameDescription.Height;

            // open the reader for the color frames
            this.colorFrameReader = this.kinectSensor.ColorFrameSource.OpenReader();

            // wire handler for frame arrival
            this.colorFrameReader.FrameArrived += this.Reader_ColorFrameArrived;

            // create the colorFrameDescription from the ColorFrameSource using Bgra format
            FrameDescription colorFrameDescription = this.kinectSensor.ColorFrameSource.CreateFrameDescription(ColorImageFormat.Bgra);

            // create the bitmap to display
            this.colorBitmap = new WriteableBitmap(colorFrameDescription.Width, colorFrameDescription.Height, 96.0, 96.0, PixelFormats.Bgr32, null);

            // open the reader for the body frames
            this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();

            // a bone defined as a line between two joints
            this.bones = new List<Tuple<JointType, JointType>>();

            // Torso
            this.bones.Add(new Tuple<JointType, JointType>(JointType.Head, JointType.Neck));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.Neck, JointType.SpineShoulder));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.SpineMid));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineMid, JointType.SpineBase));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.ShoulderRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.ShoulderLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineBase, JointType.HipRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineBase, JointType.HipLeft));

            // Right Arm
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ShoulderRight, JointType.ElbowRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ElbowRight, JointType.WristRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristRight, JointType.HandRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HandRight, JointType.HandTipRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristRight, JointType.ThumbRight));

            // Left Arm
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ShoulderLeft, JointType.ElbowLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ElbowLeft, JointType.WristLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristLeft, JointType.HandLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HandLeft, JointType.HandTipLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristLeft, JointType.ThumbLeft));

            // Right Leg
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HipRight, JointType.KneeRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.KneeRight, JointType.AnkleRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.AnkleRight, JointType.FootRight));

            // Left Leg
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HipLeft, JointType.KneeLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.KneeLeft, JointType.AnkleLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.AnkleLeft, JointType.FootLeft));

            // populate body colors, one for each BodyIndex
            this.bodyColors = new List<Pen>();

            this.bodyColors.Add(new Pen(Brushes.Red, 6));
            this.bodyColors.Add(new Pen(Brushes.Orange, 6));
            this.bodyColors.Add(new Pen(Brushes.Green, 6));
            this.bodyColors.Add(new Pen(Brushes.Blue, 6));
            this.bodyColors.Add(new Pen(Brushes.Indigo, 6));
            this.bodyColors.Add(new Pen(Brushes.Violet, 6));

            // set IsAvailableChanged event notifier
            this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;

            // open the sensor
            this.kinectSensor.Open();

            // set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.NoSensorStatusText;

            // Create the drawing group we'll use for drawing
            this.drawingGroup = new DrawingGroup();

            // Create an image source that we can use in our image control
            this.imageSource = new DrawingImage(this.drawingGroup);

            // use the window object as the view model in this simple example
            this.DataContext = this;

            // initialize the components (controls) of the window
            this.InitializeComponent();

            server = new UdpClient(19104);

            Task.Run(async () => {
                while (true) {
                    //IPEndPoint object will allow us to read datagrams sent from any source.
                    var receivedResults = await server.ReceiveAsync();
                    System.Console.WriteLine("RECEIVED IP PACKET!");
                    if (Encoding.ASCII.GetString(receivedResults.Buffer)[0] == '{') {
                        remoteIPEndPoint = receivedResults.RemoteEndPoint;
                    }
                }
            });

        }

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource {
            get {
                return this.imageSource;
            }
        }

        /// <summary>
        /// Gets or sets the current status text to display
        /// </summary>
        public string StatusText {
            get {
                return this.statusText;
            }

            set {
                if (this.statusText != value) {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null) {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }

        /// <summary>
        /// Execute start up tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e) {
            if (this.bodyFrameReader != null) {
                this.bodyFrameReader.FrameArrived += this.Reader_FrameArrived;
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e) {
            if (this.bodyFrameReader != null) {
                // BodyFrameReader is IDisposable
                this.bodyFrameReader.Dispose();
                this.bodyFrameReader = null;
            }

            if (this.kinectSensor != null) {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }

            if (this.colorFrameReader != null) {
                // ColorFrameReder is IDisposable
                this.colorFrameReader.Dispose();
                this.colorFrameReader = null;
            }
        }

        /// <summary>
        /// Handles the body frame data arriving from the sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_FrameArrived(object sender, BodyFrameArrivedEventArgs e) {
            bool dataReceived = false;



            using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame()) {
                if (bodyFrame != null) {
                    if (this.bodies == null) {
                        this.bodies = new Body[bodyFrame.BodyCount];
                    }

                    // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                    // As long as those body objects are not disposed and not set to null in the array,
                    // those body objects will be re-used.
                    bodyFrame.GetAndRefreshBodyData(this.bodies);
                    dataReceived = true;
                }
            }

            if (dataReceived) {
                using (DrawingContext dc = this.drawingGroup.Open()) {
                    // Draw a transparent background to set the render size
                    dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));
                    
                    Tuple<double, Point, HandState, HandState, Point> winner = new Tuple<double, Point, HandState, HandState, Point>(0, new Point(0, 0),HandState.Open, HandState.Open, new Point(0,0));
                    int penIndex = 0;
                    foreach (Body body in this.bodies) {
                        Pen drawPen = this.bodyColors[penIndex++];
                        double size = 0.0;

                        if (body.IsTracked) {
                            this.DrawClippedEdges(body, dc);

                            IReadOnlyDictionary<JointType, Joint> joints = body.Joints;

                            // convert the joint points to depth (display) space
                            Dictionary<JointType, Point> jointPoints = new Dictionary<JointType, Point>();

                            foreach (JointType jointType in joints.Keys) {
                                // sometimes the depth(Z) of an inferred joint may show as negative
                                // clamp down to 0.1f to prevent coordinatemapper from returning (-Infinity, -Infinity)
                                CameraSpacePoint position = joints[jointType].Position;
                                if (position.Z < 0) {
                                    position.Z = InferredZPositionClamp;
                                }

                                DepthSpacePoint depthSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(position);
                                jointPoints[jointType] = new Point(depthSpacePoint.X, depthSpacePoint.Y);
                            }
                            size = jointPoints[JointType.FootLeft].Y - jointPoints[JointType.Head].Y;
                            if (winner.Item1 < size) {
                                //System.Console.WriteLine("HERE!");
                                winner = new Tuple<double, Point, HandState, HandState, Point>(size, jointPoints[JointType.HandRight], body.HandLeftState, body.HandRightState, jointPoints[JointType.HandLeft]);
                            }
                        }
                    }
                    dc.DrawEllipse(new SolidColorBrush(Color.FromRgb(255, 255, 255)), null, winner.Item2, 20, 20);

                    // Dragging Stuff
                    Point dragProgress = new Point(0,0);
                    Point currentPOI = new Point((winner.Item2.X + winner.Item5.X) / 2,
                                                 (winner.Item2.Y + winner.Item5.Y) / 2);
                    double currentDist = new Vector(winner.Item2.X - winner.Item5.X, winner.Item2.Y - winner.Item5.Y).Length;
                    double progressScale = 1.0;
                    if (winner.Item3 == HandState.Closed && winner.Item4 == HandState.Closed) {// Dragging / Scale mode
                        if (!isDragging) {
                            isDragging = true;
                            grabStartPoint = currentPOI;
                            grabStartDist = currentDist;
                        }
                        dragProgress.X = currentPOI.X - grabStartPoint.X;
                        dragProgress.Y = currentPOI.Y - grabStartPoint.Y;
                        progressScale = Math.Abs(currentDist - grabStartDist) / grabStartDist > 0.2 ? ((currentDist / grabStartDist) + 1) / 2.0 : 1.0;
                        if (Math.Abs(currentDist - grabStartDist) / grabStartDist > 0.2 && !isScaling) {
                            isScaling = true;
                            grabStartDist = currentDist;
                        }
                        if (isScaling) {
                            progressScale = ((currentDist / grabStartDist) + 1) / 2.0;
                        }
                    } else {
                        if (isDragging) {
                            isDragging = false;
                            imageLocation.X += currentPOI.X - grabStartPoint.X;
                            imageLocation.Y += currentPOI.Y - grabStartPoint.Y;
                            if (isScaling) {
                                imageScale *= ((currentDist / grabStartDist) + 1) / 2.0;
                                //imageScale *= imageScale > 1 ? 1 / 1.1 : (imageScale < 1 ? 1.1 : 1);
                            }
                            isScaling = false;
                        }
                    }
                    //System.Console.WriteLine(progressScale);
                    // Other Stuff
                                        if (image != null) {
                        dc.DrawImage(image, new Rect(imageLocation.X + dragProgress.X  * imageScale * progressScale,
                                                     imageLocation.Y + dragProgress.Y  * imageScale * progressScale,
                                                     200 * imageScale * progressScale,
                                                     image.Height / image.Width * 200 * imageScale * progressScale));
                    }
                    /* TODO Below center image scaling functions properly but breaks color position
                    if (image != null) {
                        dc.DrawImage(image, new Rect(imageLocation.X + dragProgress.X - 200                              / 2 * imageScale * progressScale,
                                                     imageLocation.Y + dragProgress.Y - image.Height / image.Width * 200 / 2 * imageScale * progressScale,
                                                     200 * imageScale * progressScale,
                                                     image.Height / image.Width * 200 * imageScale * progressScale));
                    }
                     */
                    // Draw Bodies -- no additional processing, that gets done earlier
                    penIndex = 0;
                    foreach (Body body in this.bodies) {
                        Pen drawPen = this.bodyColors[penIndex++];
                        if (body.IsTracked) {
                            this.DrawClippedEdges(body, dc);

                            IReadOnlyDictionary<JointType, Joint> joints = body.Joints;

                            // convert the joint points to depth (display) space
                            Dictionary<JointType, Point> jointPoints = new Dictionary<JointType, Point>();

                            foreach (JointType jointType in joints.Keys) {
                                // sometimes the depth(Z) of an inferred joint may show as negative
                                // clamp down to 0.1f to prevent coordinatemapper from returning (-Infinity, -Infinity)
                                CameraSpacePoint position = joints[jointType].Position;
                                if (position.Z < 0) {
                                    position.Z = InferredZPositionClamp;
                                }

                                DepthSpacePoint depthSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(position);
                                jointPoints[jointType] = new Point(depthSpacePoint.X, depthSpacePoint.Y);
                            }
                            this.DrawBody(joints, jointPoints, dc, drawPen);

                            this.DrawHand(body.HandLeftState, jointPoints[JointType.HandLeft], dc);
                            this.DrawHand(body.HandRightState, jointPoints[JointType.HandRight], dc);
                        }
                    }
                    if (image != null) {
                        Point imageCoord = new Point((winner.Item2.X - imageLocation.X) / (200.0*imageScale) * image.PixelWidth,
                            (winner.Item2.Y - imageLocation.Y) / (image.Height / image.Width * (200.0 * imageScale)) * image.PixelHeight);
                        if (imageCoord.X >= 0 && imageCoord.X <= image.PixelWidth && imageCoord.Y >= 0 && imageCoord.Y <= image.PixelHeight) {
                            Color c = GetPixelColor(image, (int)imageCoord.X, (int)imageCoord.Y);
                            //System.Console.WriteLine(c);
                            SendColor(c);
                        } else {
                            SendColor(Colors.Black);
                        }
                    } else {
                        SendColor(Colors.Black);
                    }

                    if (winner.Item3 == HandState.Lasso) {
                        if (imageStartTrigger == false) {
                            imageStartTrigger = true;
                        } else {
                            
                        }
                    } else if (winner.Item3 == HandState.Open || winner.Item3 == HandState.Closed) {
                        if (imageStartTrigger == true) {
                            imageStartTrigger = false;
                            
                            last = null;
                            imagesCaptured = 0;
                            string time = System.DateTime.Now.ToString("hh'-'mm'-'ss", CultureInfo.CurrentUICulture.DateTimeFormat);

                            string myPhotos = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

                            string path = Path.Combine(myPhotos, "KinectScreenshot-Color-" + time + ".jpg");
                            finalImage.Save(path);
                            finalImage = null;
                        }
                    }

                    if (imageStartTrigger == true) {
                        imagesCaptured++;
                    }





                    // Speed Graph
                    if (oldPos != null) {
                        double length = (winner.Item2 - oldPos).Value.Length * 45;
                        dc.DrawRectangle(new SolidColorBrush(Colors.White), null, new Rect(10, 350-length, 35, length));
                        for (int i = 0; i < 32; ++i) {
                            int len = (i % 2 == 0 ? 1 : 0) + (i % 4 == 0 ? 1 : 0) + (i % 8 == 0 ? 1 : 0) + (i % 16 == 0 ? 2 : 0) + 1;
                            dc.DrawLine(new Pen(new SolidColorBrush(Colors.Red), 2.0), new Point(40, 350 - i * 10), new Point(40+len*5, 350 - i * 10));
                        }
                    }

                    // prevent drawing outside of our render area
                    this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));
                    foreach (Point p in trail){
                        dc.DrawEllipse(new SolidColorBrush(Color.FromArgb(128, 255, 255, 255)), null, p, 5, 5);
                    }
                    trail.Enqueue(winner.Item2);
                    if (trail.Count > 100) {
                        trail.Dequeue();
                    }
                    oldPos = winner.Item2;
                }
            }
        }


        private void SendColor(Color c) {
            // + new String(' ', 500)
            string pkt = "{\"r\":" + c.R.ToString() + ",\"g\":" + c.G.ToString() + ",\"b\":" + c.B.ToString() + "}";
            //System.Console.WriteLine(pkt);
            byte[] data = Encoding.ASCII.GetBytes(pkt);
            if (remoteIPEndPoint != null) {
                server.Send(data, data.Length, remoteIPEndPoint);
            }
        }
        /// <summary>
        /// Draws a body
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// <param name="drawingPen">specifies color to draw a specific body</param>
        private void DrawBody(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, DrawingContext drawingContext, Pen drawingPen) {
            // Draw the bones
            foreach (var bone in this.bones) {
                this.DrawBone(joints, jointPoints, bone.Item1, bone.Item2, drawingContext, drawingPen);
            }

            // Draw the joints
            foreach (JointType jointType in joints.Keys) {
                Brush drawBrush = null;

                TrackingState trackingState = joints[jointType].TrackingState;

                if (trackingState == TrackingState.Tracked) {
                    drawBrush = this.trackedJointBrush;
                } else if (trackingState == TrackingState.Inferred) {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null) {
                    drawingContext.DrawEllipse(drawBrush, null, jointPoints[jointType], JointThickness, JointThickness);
                }
            }
        }

        /// <summary>
        /// Draws one bone of a body (joint to joint)
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="jointType0">first joint of bone to draw</param>
        /// <param name="jointType1">second joint of bone to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// /// <param name="drawingPen">specifies color to draw a specific bone</param>
        private void DrawBone(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, JointType jointType0, JointType jointType1, DrawingContext drawingContext, Pen drawingPen) {
            Joint joint0 = joints[jointType0];
            Joint joint1 = joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == TrackingState.NotTracked ||
                joint1.TrackingState == TrackingState.NotTracked) {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if ((joint0.TrackingState == TrackingState.Tracked) && (joint1.TrackingState == TrackingState.Tracked)) {
                drawPen = drawingPen;
            }

            drawingContext.DrawLine(drawPen, jointPoints[jointType0], jointPoints[jointType1]);
        }

        /// <summary>
        /// Draws a hand symbol if the hand is tracked: red circle = closed, green circle = opened; blue circle = lasso
        /// </summary>
        /// <param name="handState">state of the hand</param>
        /// <param name="handPosition">position of the hand</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawHand(HandState handState, Point handPosition, DrawingContext drawingContext) {
            switch (handState) {
                case HandState.Closed:
                    drawingContext.DrawEllipse(this.handClosedBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Open:
                    drawingContext.DrawEllipse(this.handOpenBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Lasso:
                    drawingContext.DrawEllipse(this.handLassoBrush, null, handPosition, HandSize, HandSize);
                    break;
            }
        }

        /// <summary>
        /// Draws indicators to show which edges are clipping body data
        /// </summary>
        /// <param name="body">body to draw clipping information for</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawClippedEdges(Body body, DrawingContext drawingContext) {
            FrameEdges clippedEdges = body.ClippedEdges;

            if (clippedEdges.HasFlag(FrameEdges.Bottom)) {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, this.displayHeight - ClipBoundsThickness, this.displayWidth, ClipBoundsThickness));
            }

            if (clippedEdges.HasFlag(FrameEdges.Top)) {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, this.displayWidth, ClipBoundsThickness));
            }

            if (clippedEdges.HasFlag(FrameEdges.Left)) {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, ClipBoundsThickness, this.displayHeight));
            }

            if (clippedEdges.HasFlag(FrameEdges.Right)) {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(this.displayWidth - ClipBoundsThickness, 0, ClipBoundsThickness, this.displayHeight));
            }
        }

        /// <summary>
        /// Handles the event which the sensor becomes unavailable (E.g. paused, closed, unplugged).
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e) {
            // on failure, set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.SensorNotAvailableStatusText;
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".jpg"; // Default file extension
            dlg.Filter = "Images (.jpg)|*.jpg"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result == true) {
                // Open document 
                string filename = dlg.FileName;
                image = new BitmapImage(new Uri("file://" + filename));
                imageScale = 1.0;
                imageLocation = new Point(200, 10);
            }
        }



        private void Reader_ColorFrameArrived(object sender, ColorFrameArrivedEventArgs e) {
            // ColorFrame is IDisposable
            using (ColorFrame colorFrame = e.FrameReference.AcquireFrame()) {
                if (colorFrame != null) {
                    FrameDescription colorFrameDescription = colorFrame.FrameDescription;

                    using (KinectBuffer colorBuffer = colorFrame.LockRawImageBuffer()) {
                        this.colorBitmap.Lock();

                        // verify data and write the new color frame data to the display bitmap
                        if ((colorFrameDescription.Width == this.colorBitmap.PixelWidth) && (colorFrameDescription.Height == this.colorBitmap.PixelHeight)) {
                            colorFrame.CopyConvertedFrameDataToIntPtr(
                                this.colorBitmap.BackBuffer,
                                (uint)(colorFrameDescription.Width * colorFrameDescription.Height * 4),
                                ColorImageFormat.Bgra);
                            
                            //this.colorBitmap.AddDirtyRect(new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight));
                        }
                        this.colorBitmap.Unlock();
                        //Emgu.CV.Matrix<Byte> mat = new Matrix<byte>(colorFrameDescription.Height, colorFrameDescription.Width, this.colorBitmap.BackBuffer);
                        //System.Drawing.Bitmap bmp = BitmapFromWriteableBitmap(this.colorBitmap);
                        Image<Bgra, byte> img = new Image<Bgra, byte>(colorFrameDescription.Width,colorFrameDescription.Height,this.colorBitmap.BackBufferStride,this.colorBitmap.BackBuffer);
                        //img = img.Mul(0.1);
                        if (finalImage == null) {
                            finalImage = img;
                        } else if (last != null) {
                            finalImage = finalImage + img;
                        }
                        last = img.Copy();
                    }
                }
            }
        }
    }
}
